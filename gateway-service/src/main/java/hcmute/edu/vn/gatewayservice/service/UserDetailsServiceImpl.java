package hcmute.edu.vn.gatewayservice.service;





import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import hcmute.edu.vn.gatewayservice.entity.User;
import hcmute.edu.vn.gatewayservice.security.UserPrinciple;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	 
	  @Autowired
	  NoneUserServiceProxy noneUserServiceProxy;
	 
	  @Override
	  public UserPrinciple loadUserByUsername(String email) throws UsernameNotFoundException {
	 
	    User user = noneUserServiceProxy.checkUser(email).getData();
	    if(user.getId() == null) {
	    	throw new UsernameNotFoundException("User Not Found with -> username or email : " + email);
	    }  			 
	    return UserPrinciple.build(user);
	  }
	}