package hcmute.edu.vn.gatewayservice.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import hcmute.edu.vn.gatewayservice.entity.User;



public class UserPrinciple implements UserDetails{

	private static final long serialVersionUID = 1L;

	private Long id;

	private String email;

	@JsonIgnore
	private String password;

	private Collection<? extends GrantedAuthority> authorities;
	
	public UserPrinciple() {
		
	}
	
	public UserPrinciple(Long id, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;		
		this.email = email;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserPrinciple build(User user) {
		Set<GrantedAuthority> authorities = new HashSet<>();
		user.getRoles().stream().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getPermission())));
//				user.getRoles().stream()
//				.map(role -> new SimpleGrantedAuthority(role.getPermission())).collect(Collectors.toList());
		return new UserPrinciple(user.getId(), user.getEmail(), user.getPassword(), authorities);
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {		
		return authorities;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getPassword() {		
		return password;
	}

	@Override
	public String getUsername() {	
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {		
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {		
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {		
		return true;
	}

	@Override
	public boolean isEnabled() {		
		return true;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserPrinciple user = (UserPrinciple) o;
		return Objects.equals(id, user.id);
	}
	
}
