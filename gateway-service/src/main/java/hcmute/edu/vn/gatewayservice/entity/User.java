package hcmute.edu.vn.gatewayservice.entity;

import java.util.Date;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor

public class User {
	
    private Long id;
  
    private String email;

    private String password;

    private int status;

    private String firstName;

    private String lastName;

    private Date dateOfBirth;

    private int sex;
   
    private String avatar;

    private String address;

    private String phone;

    private Date dateCreated;

    private String userCreated;

    private Date dateUpdated;

    private String userUpdated;

    private Set<Role> roles;



}




