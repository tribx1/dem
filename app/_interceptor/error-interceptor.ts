import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NuServiceService } from '../_service/nu_service/nu-service.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private nuServiceService: NuServiceService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(catchError(err => {
       
        console.log(err);
       
        // if (err.status === 401 || err.status === 403) {
        //   // auto logout if 401 response returned from api
        //   this.nuServiceService.Logout();
        //   return         
        // }
        const error = err.error.message || err.statusText;
        return throwError(error);
      }));
  }
}
