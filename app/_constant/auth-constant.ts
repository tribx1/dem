export const AuthorizationConstant = {
    ROLE_ADMIN: 'ROLE_ADMIN',
    ROLE_USER: 'ROLE_USER'
}

export const AuthenticationConstant = {
    Authorization: 'Authorization'
}

export const TokenConstant = {
    X_ACCESS_TOKEN: 'access_token',
    X_SUB_TOKEN: 'sub_token'
}

export const PREFIX = {
    ROLE: 'ROLE_',
    ROLE_LENGTH: 5
}