import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';


import * as $ from 'jquery';

import { ModServiceService } from '../_service/mod_service/mod-service.service';
import { Router } from '@angular/router';
import { Item } from '../_entity/item';
import { Role } from '../_entity/role';
import { first } from 'rxjs/operators';
import { UserService } from '../_service/user_service/user.service';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { Cat } from '../_entity/cat';
import { CatItem } from '../_entity/catitem';
import { Guest } from '../_entity/guest';
import { MatTableDataSource } from '@angular/material/table';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-news-management',
  templateUrl: './news-management.component.html',
  styleUrls: ['./news-management.component.css']
})
export class NewsManagementComponent implements OnInit {

  items: Item[];
  dataTable: any;  
  email: string
  isMod: boolean = false
  rolesofUser: Role[]
  cats: Cat[]
  catItems: CatItem[]
  select: number
  catId: number
  pass: string
  error: string
  isAdmin: boolean = false
  guest: Guest = new Guest();
  displayedColumns: string[] = ['title', 'shortDesc', 'author', 'status'];
  dataSource = new MatTableDataSource(this.catItems);
  length = 100;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  constructor(private router: Router, private modService: ModServiceService, private chRef: ChangeDetectorRef,
    private userService: UserService, private nuService: NuServiceService) { }

  ngOnInit() {

    this.checkEmail()
  }

  async getListItem() {
    await this.getCatOfUser()

  }
  async getCatOfUser() {
    await this.checkEmail()
    console.log(this.isAdmin)


  }
  onGotoItemDetail(id) {

    window.location.href = "/newsdetail/" + id;
  }

  onChangeCatSelect(e) {
    this.pageIndex = 0;
    this.modService.getItemByCatId(this.select, this.pageSize, this.pageIndex)
      .subscribe(res => {
        if (res.success == "true") {          
          this.catItems = res.data;
          this.dataSource.data = this.catItems;
          this.length = res.totalElement;
        }     
      }, err => {
        console.log("err.message");     
      });
  }

  onChangePage(event?: PageEvent) {   
    this.modService.getItemByCatId(this.select, event.pageSize, event.pageIndex)
      .subscribe(res => {
        if (res.success == "true") {          
          this.catItems = res.data;
          this.dataSource.data = this.catItems;
          this.length = res.totalElement;
        }       
      }, err => {
        console.log("err.message");       
      });
  }


  checkEmail() {
    this.email = localStorage.getItem("email")
    if (this.email == null) {
      // this.router.navigate(["/"])
      window.location.href = "/";
    }
    this.userService.getProfile(this.email)
      .pipe(first())
      .subscribe(res => {
        if (res.success == "true") {
          this.rolesofUser = res.data.roles         
          for (let role of this.rolesofUser) {
            if (role.p_admin == true && role.status == 1) {
              this.isAdmin = true
            }
            if (role.p_delete == true || role.p_approve == true) {
              if (role.status == 1) {
                this.isMod = true;
              }
            }
          }
          if (this.isAdmin == false && this.isMod == false) {

            alert("Bạn không được truy cập vào trang này")
            // this.router.navigate(["/"])
            window.location.href = "/";

          } else {
            if (this.isAdmin == true) {
              this.modService.getAllCat()
                .subscribe(res => {
                  if (res.success == "true") {
                    this.cats = res.data;
                    this.catId = this.cats[0].id
                  }
                  this.select = this.catId
                  this.modService.getItemByCatId(this.catId, this.pageSize, this.pageIndex)
                    .subscribe(res => {
                      if (res.success == "true") {
                        this.catItems = res.data;
                        this.dataSource.data = this.catItems;
                        this.length = res.totalElement;
                      }
                    }, err => {
                      console.log(err.message)
                    });

                }, err => {
                  console.log(err.message)
                });

            } else {
              this.email = localStorage.getItem("email")
              this.modService.getAllCatofUser(this.email)
                .subscribe(res => {
                  if (res.success == "true") {

                    this.cats = res.data;
                    this.catId = this.cats[0].id

                  }
                  this.select = this.catId
                  this.modService.getItemByCatId(this.catId,  this.pageSize, this.pageIndex)
                    .subscribe(res => {
                      if (res.success == "true") {
                        this.catItems = res.data;
                        this.dataSource.data = this.catItems;
                        this.length = res.totalElement;
                      }
                    
                    }, err => {
                      console.log(err.message)
                    });

                }, err => {
                  console.log(err.message)
                });
            }


          }


        }
        else {
          this.error = res.message
        }
      }, err => {
        console.log(err)
      })

  }

}
