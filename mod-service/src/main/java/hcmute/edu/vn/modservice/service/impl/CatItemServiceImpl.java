package hcmute.edu.vn.modservice.service.impl;

import hcmute.edu.vn.modservice.exception.NotFoundException;
import hcmute.edu.vn.modservice.model.Cat_Item;
import hcmute.edu.vn.modservice.repository.CatItemRepository;
import hcmute.edu.vn.modservice.service.CatItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CatItemServiceImpl implements CatItemService {

	@Autowired
	CatItemRepository catItemRepository;

	@Override
	public CrudRepository<Cat_Item, Long> getRepo() {
		return catItemRepository;
	}

	@Override
	public List<Cat_Item> retrieveAllCatItem(Long id) {

		return null;
	}

	@Override
	public List<Cat_Item> retrieveAllByCatId(Long id) {

		List<Cat_Item> cat_items = catItemRepository.findById_Cat_Id(id);
		if (cat_items.isEmpty())
			throw new NotFoundException("Not Found Product in Your Cart");
		return cat_items;
	}

	@Override
	public Page<Cat_Item> retrieveAllByCatId(Long id, Pageable pageable) {
		Page<Cat_Item> catItem;
		List<Integer> statusList = new ArrayList<Integer>();
		statusList.add(1);
		statusList.add(2);
		catItem = catItemRepository.findById_Cat_IdAndId_Item_StatusIn(id, statusList, pageable);
		if (catItem.getContent().isEmpty()) {
			throw new NotFoundException("List Item not found");
		}
		return catItem;
	}
}
